import React, { useEffect, useState } from "react";
import { Modal, Button } from "react-bootstrap";


function ConferenceList() {
    const [conferences, setConferences] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [deleteId, setDeleteId] = useState(null);

    const getData = async () => {
        const request = await fetch('http://localhost:8000/api/conferences/')
        if (request.ok) {
            const data  = await request.json()
            setConferences(data.conferences)
        } else {
            console.error('Request Error')
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleDelete = (id) => {
        setDeleteId(id);
        setShowModal(true);
    };

    const confirmDelete = async () => {
        const request = await fetch(`http://localhost:8000/api/conferences/${deleteId}`, { method: "DELETE"}
        );
        const response = await request.json();
        getData();
        setShowModal(false);
    };

    const handleCloseModal = () => {
        setShowModal(false);
    }


    return (
        <div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Max Attendees</th>
                <th>Location</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {conferences.sort((a,b) => (a.id -b.id)).map(conference => {
                return (
                <tr key={ conference.href }>
                    <td>{ conference.id }</td>
                    <td>{ conference.name }</td>
                    <td>{ conference.max_attendees }</td>
                    <td>{ conference.location.name }</td>
                    <td><button onClick={()=> handleDelete(conference.id)} className="btn btn-danger">Delete</button></td>
                </tr>
                );
            })}
            </tbody>
        </table>
        <Modal show={showModal} onHide={handleCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>Confirm Deletion</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Are you sure you want to delete this item?
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseModal}>
                  Cancel
                </Button>
                <Button variant="danger" onClick={confirmDelete}>
                    Delete
                </Button>
            </Modal.Footer>
        </Modal>
        </div>
    );
}

export default ConferenceList;
