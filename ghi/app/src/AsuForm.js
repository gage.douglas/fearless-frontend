import React, { useEffect, useState } from 'react';


function AsuForm() {
    const [conferences, setConferences] = useState([]);
    const [loading, setLoading] = useState(true);
    const [isFormSubmitted, setFormSubmitted] = useState(false);

    // Change Name form event handling
    const [name, setName] = useState("");
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    // Change Email form event handling
    const [email, setEmail] = useState("");
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    // Change Conference selection form event handling
    const [conference, setConference] = useState("");
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    } catch (error) {
        console.error('Error fetching conference data:', error);
    } finally {
        setLoading(false);
    }
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.email = email;
        data.conference = conference;
        console.log(data);

        const attendeeUrl = 'http://localhost:8001/api/conferences/1/attendees/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            setName('');
            setEmail('');
            setConference('');
            setFormSubmitted(true);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className='container'>
        <div className='row'>
          <div className='col-4'>
            <div className='shadow mt-2'>
              <img src="/logo.svg" alt='Logo' />
            </div>
          </div>
          <div className='col-lg-6'>
            {isFormSubmitted ? (
              <div className='shadow p-4 mt-2'>
                <div className='alert alert-success' role='alert'>
                  Congratulations! You're all signed up. That's right, BEOTCH!
                </div>
              </div>
            ) : (
              <div className='shadow p-4 mt-2'>
                <h1>It's Conference Time!</h1>
                <p>Please choose which conference you'd like to attend.</p>

                {loading ? (
                  <div className='d-flex justify-content-center mb-3'>
                    <div className='spinner-border' role='status'>
                      <span className='visually-hidden'>Loading...</span>
                    </div>
                  </div>
                ) : (
                  <form onSubmit={handleSubmit} id="create-attendee-form" role='status'>
                    <div className='mb-3'>
                      <select onChange={handleConferenceChange} value={conference} required name='conference' id='conference' className='form-select'>
                        <option value=''>Choose a conference</option>
                        {conferences.map(conference => (
                          <option key={conference.href} value={conference.href}>
                            {conference.name}
                          </option>
                        ))}
                      </select>
                    </div>
                    <div className='form-group row'>
                      <p>Now, tell us about yourself.</p>
                      <div className='col-lg-6'>
                        <input onChange={handleNameChange} value={name} placeholder='Your full name' required type='text' name='name' id='name' className='form-control' />
                      </div>
                      <div className='col-lg-6'>
                        <input onChange={handleEmailChange} value={email} placeholder='Your email address' required type='text' name='email' id='email' className='form-control' />
                      </div>
                    </div>
                    <button type='submit' className='btn btn-primary mt-3'>I'm going!</button>
                  </form>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    );
}
export default AsuForm;
