import React, { useEffect, useState } from "react";


function PresentationForm() {
    const [conferences, setConferences] = useState([]);

    // Change Name form event handling
    const [presenterName, setName] = useState("");
    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }

    const [presenterEmail, setEmail] = useState("");
    const handleEmailChange = (e) => {
        const value = e.target.value;
        setEmail(value);
    }

    const [companyName, setCompany] = useState("");
    const handleCompanyChange = (e) => {
        const value = e.target.value;
        setCompany(value);
    }

    const [title, setTitle] = useState("");
    const handleTitleChange = (e) => {
        const value = e.target.value;
        setTitle(value);
    }

    const[synopsis, setSynopsis] = useState("");
    const handleSynopsisChange = (e) => {
        const value = e.target.value;
        setSynopsis(value);
    }

    const [conference, setConference] = useState("");
    const handleConferenceChange = (e) => {
        const value = e.target.value;
        setConference(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        // line injection of presentationURL
        const confId = data.conference;

        const presentationURL = `http://localhost:8000${confId}presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationURL, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            setName('');
            setEmail('');
            setCompany('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new presentation!</h1>
        <form onSubmit={handleSubmit} id="create-presentation-form">
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} value={presenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
            <label htmlFor="presenter_name">Presenter name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleEmailChange} value={presenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
            <label htmlFor="presenter_email">Presenter email</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleCompanyChange} value={companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
            <label htmlFor="company_name">Company name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
            <label htmlFor="title">Title</label>
          </div>
          <div className="mb-3">
            <label htmlFor="synopsis">Synopsis</label>
            <textarea onChange={handleSynopsisChange} value={synopsis} id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
          </div>
          <div className="mb-3">
          <select onChange={handleConferenceChange} value={conference} required name='conference' id='conference' className='form-select'>
            <option value=''>Choose a conference</option>
            {conferences.map(conference => (
                <option key={conference.href} value={conference.href}>
                {conference.name}
                </option>
            ))}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
);
}
export default PresentationForm;
