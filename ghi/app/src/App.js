import { createBrowserRouter, RouterProvider, Outlet} from 'react-router-dom';
import Nav from './Nav';
import './App.css';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AsuForm from './AsuForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import ConferenceList from './ConferenceList';

function App(props) {
  const router = createBrowserRouter([
    {
      path: "/",
      element: (
        <>
        <Nav />
          <Outlet />
      </>
      ),
      children: [
        {
          index: 'home',
          children: [
            { index: true, element: <MainPage /> },
          ]
        },
        {
          path: "locations",
          children: [
            { path: "new", element: <LocationForm /> },
          ]
        },
        {
          path: "conferences",
          children: [
            {path: "new", element: <ConferenceForm />},
            {path: "", element: <ConferenceList />},
          ]
        },
        {
          path: "attendees",
          children: [
            {path: "new", element: <AsuForm />},
            {path: "", element: <AttendeesList attendees={props.attendees} />},
          ]
        },
        {
          path: "presentations",
          children: [
            {path: "new", element: <PresentationForm />}
          ]
        }
      ]
    }
  ])
return <RouterProvider router={router} />;
}

export default App;
